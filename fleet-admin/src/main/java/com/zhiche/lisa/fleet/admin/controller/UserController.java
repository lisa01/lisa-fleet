package com.zhiche.lisa.fleet.admin.controller;

import com.zhiche.lisa.fleet.model.User;
import com.zhiche.lisa.fleet.service.UserService;
import com.zhiche.lisa.fleet.supports.RestfulResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zhaoguixin on 2018/10/16.
 */
@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/listUser")
    public RestfulResponse<List<User>> queryPage() {
        RestfulResponse<List<User>> response = new RestfulResponse<>(0, "success", null);
        List<User> userList = userService.lisaUser();
        response.setData(userList);
        return response;
    }
}
