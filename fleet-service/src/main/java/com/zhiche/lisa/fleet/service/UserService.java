package com.zhiche.lisa.fleet.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.lisa.fleet.mapper.UserMapper;
import com.zhiche.lisa.fleet.model.User;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
@Service
public class UserService extends ServiceImpl<UserMapper, User> {

    public List<User> lisaUser() {
        return baseMapper.listUser();
    }
}
