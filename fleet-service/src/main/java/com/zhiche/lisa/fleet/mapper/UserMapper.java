package com.zhiche.lisa.fleet.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.lisa.fleet.model.User;


import java.util.List;

/**
 * <p>
 * 用户 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface UserMapper extends BaseMapper<User> {

    List<User> listUser();
}
