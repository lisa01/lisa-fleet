package com.zhiche.lisa.fleet.supports;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel
public class SimpleResult<T> implements Serializable {
    private static final long serialVersionUID = -6820865870078657785L;
    @ApiModelProperty(value = "处理成功标志")
    private boolean success;
    @ApiModelProperty("传输的数据")
    private T data;
    @ApiModelProperty(hidden = true)
    private String messageCode;
    @ApiModelProperty("返回的信息提示")
    private String message;

    private Integer code;


    public SimpleResult() {
    }

    public SimpleResult(boolean success, T data, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
    }

    public SimpleResult(boolean success, T data, String messageCode, String message) {
        this.success = success;
        this.data = data;
        this.message = message;
        this.messageCode = messageCode;
    }

    public SimpleResult(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public SimpleResult(boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return this.messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String toString() {
        String data = this.getData() == null ? "" : this.getData().toString();
        return "SimpleResult [success = " + this.isSuccess() + ",messageCode = " + this.getMessageCode() + ",message = " + this.getMessage() + ",data = {" + data + "}]";
    }
}
